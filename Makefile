# Inspired by https://github.com/archlinux/arch-security-tracker/blob/master/Makefile

PYTHON?=python
FLAKE8?=flake8
PYTEST?=py.test
PYTEST_OPTIONS+=-s
PYTEST_INPUT?=test
PYTEST_COVERAGE_OPTIONS+=--cov-report=term-missing --cov-report=html:test/coverage --cov=signoff

.PHONY: test lint

build:
	$(PYTHON) setup.py build

test: test-py

test-py coverage:
	PYTHONPATH="${BUILD_DIR}:.:${PYTHONPATH}" ${PYTEST} ${PYTEST_INPUT} ${PYTEST_OPTIONS} ${PYTEST_COVERAGE_OPTIONS}

open-coverage: coverage
	${BROWSER} test/coverage/index.html

lint:
	$(FLAKE8) signoff

from click.testing import CliRunner

import pytest

from signoff import main as entrypoint, SignoffSession

runner = CliRunner()

STANDARD_ARGS = ["--username", "test", "--password", "test"]


@pytest.fixture
def mock_get_signoffs_empty(monkeypatch):
    def mock_get_signoffs_empty(*args, **kwargs):
        return []

    monkeypatch.setattr(SignoffSession, "get_signoffs", mock_get_signoffs_empty)


@pytest.fixture
def mock_signoff_package(monkeypatch):
    def mock_signoff_package(*args, **kwargs):
        return

    monkeypatch.setattr(SignoffSession, "signoff_package", mock_signoff_package)


@pytest.fixture
def mock_revoke_package(monkeypatch):
    def mock_revoke_package(*args, **kwargs):
        return

    monkeypatch.setattr(SignoffSession, "revoke_package", mock_revoke_package)


@pytest.fixture
def mock_get_signoffs(monkeypatch):
    def mock_get_signoffs(*args, **kwargs):
        return [
            {
                "arch": "x86_64",
                "last_update": "2021-04-12T01:00:22.740Z",
                "maintainers": ["maintainer"],
                "packager": "packager",
                "pkgbase": "linux",
                "repo": "Testing",
                "signoffs": [],
                "target_repo": "Core",
                "version": "5.5.3.arch1-1",
                "pkgnames": ["linux"],
                "package_count": 1,
                "approved": False,
                "required": 2,
                "enabled": True,
                "known_bad": False,
                "comments": "new release"
            }
        ]

    monkeypatch.setattr(SignoffSession, "get_signoffs", mock_get_signoffs)


@pytest.fixture
def mock_login(monkeypatch):
    def mock_login(*args, **kwargs):
        return None

    monkeypatch.setattr(SignoffSession, "_login", mock_login)


@pytest.fixture(scope="session")
def empty_localdb(generate_localdb):
    '''Returns the location to the local db'''

    return generate_localdb([])


@pytest.fixture(scope="session")
def localdb(generate_localdb):
    '''Returns the location to the local db'''

    data = [{
        "name": "linux",
        "base": "linux",
        "arch": "x86_64",
        "csize": "2483776",
        "version": "5.5.3.arch1-1",
        "builddate": "1573556456",
        "desc": "The linux kernel and modules",
        "url": "https://kernel.org",
        "license": "GPL2",
        "packager": "Arch Dev <developer@archlinux.org>",
        "conflicts": [],
        "replaces": [],
        "depends": ["coreutils"],
        "makedepends": ["bc"],
        "optdepends": ["vim"]
    }]
    return generate_localdb(data)


def test_no_db():
    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--list'])
    assert result.exit_code == 2


def test_list(mock_login, mock_get_signoffs, localdb):
    '''List to be signed off packages'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--list', '--db-path', localdb])
    assert result.exit_code == 0
    assert 'linux' in result.output


def test_list_empty_localdb(mock_login, mock_get_signoffs, empty_localdb):
    '''The local db is empty, signoffs contain linux'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--list', '--db-path', empty_localdb])
    assert result.exit_code == 0
    assert result.output == ''


def test_list_empty_signoffs(mock_login, mock_get_signoffs_empty, localdb):
    '''The local db contains linux, signoffs are empty'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--list', '--db-path', localdb])
    assert result.exit_code == 0
    assert result.output == ''


def test_signoff(mock_login, mock_get_signoffs, mock_signoff_package, localdb):
    '''Signoff the Linux package'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--signoff', 'linux', '--noconfirm', '--db-path', localdb])
    assert result.exit_code == 0
    assert result.output == 'Signed off linux.\n'


def test_signoff_non_existant_package(mock_login, mock_get_signoffs, mock_signoff_package, empty_localdb):
    '''Signoff a non-existant package'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--signoff', 'linux', '--noconfirm', '--db-path', empty_localdb])
    assert result.exit_code == 2
    assert 'Error: linux package not installed' in result.output


def test_revoke_non_existant_package(mock_login, mock_get_signoffs, mock_revoke_package, empty_localdb):
    '''Revoke non-existant package'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--revoke', 'linux', '--noconfirm', '--db-path', empty_localdb])
    assert result.exit_code == 2
    assert 'Error: linux package not installed' in result.output


def test_revoke(mock_login, mock_get_signoffs, mock_revoke_package, localdb):
    '''Revoke non-existant package'''

    result = runner.invoke(entrypoint, STANDARD_ARGS + ['--revoke', 'linux', '--noconfirm', '--db-path', localdb])
    assert result.exit_code == 0
    assert result.output == 'Revoked sign-off for linux.\n'
